<?php

/**
 * @file
 * Drush integration of fzcd.
 *
 * drush fzcd-st - Show the Free Zip Code Database module status.
 */

/**
 * Implement hook_drush_help().
 */
function fzcd_drush_help($section) {
  switch ($section) {
    case 'drush:fzcd-st':
      return dt('Show the Free Zip Code Database module status.');
    case 'drush:fzcd-zip':
      return dt('Get US state by zip code.');
    case 'drush:fzcd-city':
      return dt('Get US state by city name.');
    case 'drush:fzcd-city-zip':
      return dt('Get zip by city name.');
  }
}

/**
 * Implement hook_drush_command().
 */
function fzcd_drush_command() {
  $items = array();

  $items['fzcd-st'] = array(
    'description' => 'Show the Free Zip Code Database module status.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );
  $items['fzcd-zip'] = array(
    'drupal dependencies' => array('fzcd'),
    'description' => 'Get US state by zip code.',
    'arguments' => array(
      'zip' => 'A zip code.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'examples' => array(
      'drush fzcd-state 10001' => 'Get US state by zip code.',
    ),
  );
  $items['fzcd-city'] = array(
    'drupal dependencies' => array('fzcd'),
    'description' => 'Get US states by city name.',
    'arguments' => array(
      'city' => 'City.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'examples' => array(
      'drush fzcd-city Liverpool' => 'Get US state by city name.',
    ),
  );
  $items['fzcd-city-zip'] = array(
    'drupal dependencies' => array('fzcd'),
    'description' => 'Get zip by city name.',
    'arguments' => array(
      'city' => 'City.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'examples' => array(
      'drush fzcd-city-zip Liverpool' => 'Get zip by city name.',
    ),
  );

  return $items;
}

/**
 * Callback function for fzcd-st command.
 */
function drush_fzcd_st() {
  drush_print(dt('Free Zip Code Database:'));
  $is_table_exists = db_table_exists('fzcd');
  drush_print(dt('  schema: @status', array('@status' => $is_table_exists ? 'installed' : 'DO NOT installed')));
  if ($is_table_exists) {
    drush_print(dt('  database records: @num', array('@num' => db_select('fzcd')->countQuery()->execute()->fetchField())));
  }
}

/**
 * Callback function for fzcd-zip command.
 */
function drush_fzcd_zip() {
  $zip = func_get_args();
  if (empty($zip)) {
    return drush_set_error(dt('No zip code provided.'));
  }
  $zip = $zip[0];
  if (!preg_match('/^\d{3,5}$/', $zip)) {
    return drush_set_error(dt('Please provide 5-digits zip code.'));
  }
  if ($state = fzcd_get_state_by_zip($zip)) {
    return drush_print($state);
  }
  return drush_set_error(dt('State not found.'));
}

/**
 * Callback function for fzcd-city command.
 */
function drush_fzcd_city() {
  $city = func_get_args();
  if (empty($city)) {
    return drush_set_error(dt('No city provided.'));
  }
  $city = $city[0];
  if ($state = fzcd_get_state_by_city($city)) {
    return drush_print(implode(', ', $state));
  }
  return drush_set_error(dt('State not found.'));
}

/**
 * Callback function for fzcd-city-zip command.
 */
function drush_fzcd_city_zip() {
  $city = func_get_args();
  if (empty($city)) {
    return drush_set_error(dt('No city provided.'));
  }
  $city = $city[0];
  if ($zip = fzcd_get_zip_by_city($city)) {
    return drush_print(implode(', ', $zip));
  }
  return drush_set_error(dt('Zip not found.'));
}
