<?php

/**
 * @file
 * Administration page callbacks for the Free ZIP Code Database module.
 */
define('FZCD_ONE_TIME_INSERT_RECORDS_LIMIT', 10000);

/**
 * Page callback: Displays the status page.
 */
function fzcd_admin_overview() {
  $output = '';
  $last_update = variable_get('fzcd_last_update', FALSE);
  $total = variable_get('fzcd_zip_count', 0);
  $output .= '<p>';
  if ($last_update) {
    $output = t('Last update - @date, ZIP Codes - !total', array('@date' => format_date($last_update), '!total' => $total));
  }
  else {
    $output = t('Update never run.');
  }
  $output .= '</p>';
  $output .= '<p>';
  $output .= l(t('Import new ZIP Code data now.'), 'admin/config/services/fzcd/import');
  $output .= '</p>';

  return $output;
}

/**
 * Form constructor for the fzcd data import.
 *
 * @see fzcd_admin_form_submit(),fzcd_admin_form_validate()
 * @ingroup forms
 */
function fzcd_admin_form($form, $form_state) {
  $form['#attributes']['enctype'] = 'multipart/form-data';

  $form['info'] = array(
    '#markup' => t('Download the Free ZIP Code Database from !link, unzip and insert csv file into field below.', array('!link' => l('greatdata.com', 'http://greatdata.com/free-zip-code-database'))),
  );

  $form['file'] = array(
    '#title' => 'Select csv file to import',
    '#type' => 'file',
    '#description' => t('Note: existing ZIP Code Database will be replaced with new one.'),
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import new data'),
  );

  return $form;
}

/**
 * Form submission validator for fzcd_admin_form().
 */
function fzcd_admin_form_validate($form, &$form_state) {
  $file = FALSE;
  if (isset($form['file'])) {
    $validators = array('file_validate_extensions' => array('csv'));

    $dir = 'temporary://';

    $file = file_save_upload('file', $validators, $dir, FILE_EXISTS_REPLACE);

    if ($file) {
      $form_state['values']['file'] = $file;
    }
  }
  if ($file) {
    _fzcd_import_data($file);
  }
  else {
    form_set_error('file', t('Please select csv file.'));
  }
}

/**
 * Form submission handler for fzcd_admin_form().
 */
function fzcd_admin_form_submit($form, &$form_state) {
  variable_set('fzcd_last_update', time());
}

/**
 * Import data from uploaded scv file into databse
 * 
 * @param drupal file object $file
 */
function _fzcd_import_data($file) {
  $filepath = drupal_realpath($file->uri);
  if ($filepath) {
    $data = file_get_contents($filepath);
    $data = iconv('ISO-8859-1', 'UTF-8', $data);
    $data = explode("\n", $data);
    if (is_array($data) && count($data) > 1) {
      $limit = 0;
      $total = 0;
      $query = db_insert('fzcd')->fields(array('zip', 'state', 'city', 'county', 'lat', 'lon'));
      // remove headers
      unset($data[0]);
      foreach ($data as $key => $value) {
        $value = explode(',', $value);
        if (is_array($value) and count($value) == 6) {
          $total++;
          $limit++;
          $query->values(array(
            'zip' => (int) $value[0],
            'state' => strtoupper($value[1]),
            'city' => $value[2],
            'county' => $value[3],
            'lat' => (float) $value[4],
            'lon' => (float) $value[5],
          ));
          // break long insret query into small one
          if ($limit == FZCD_ONE_TIME_INSERT_RECORDS_LIMIT) {
            if ($limit == $total) {
              // clear existing data
              db_delete('fzcd')->execute();
            }
            // reset limit counter
            $limit = 0;
            // insert new data
            $query->execute();
            $query = db_insert('fzcd')->fields(array('zip', 'state', 'city', 'county', 'lat', 'lon'));
          }
        }
      }
      if ($limit > 0) {
        if ($limit == $total) {
          // clear existing data
          db_delete('fzcd')->execute();
        }
        // insert new data
        $query->execute();
      }
      if ($total > 0) {
        variable_set('fzcd_zip_count', $total);
        drupal_set_message(t('Inserted @total ZIP-codes', array('@total' => $total)));
      }
    }
  }
}
