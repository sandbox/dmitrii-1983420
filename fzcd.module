<?php

/**
 * @file
 * Free ZIP Code Database module.
 */

/**
 * Implements hook_permission().
 */
function fzcd_permission() {
  return array(
    'administer fzcd' => array(
      'title' => t('Administer Free ZIP Code Database'),
      'description' => t('Perform administration tasks for Free ZIP Code Database module.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function fzcd_menu() {
  $items['admin/config/services/fzcd'] = array(
    'title' => 'Free ZIP Code Database',
    'description' => "Free ZIP Code Database module statistics.",
    'page callback' => 'fzcd_admin_overview',
    'access arguments' => array('administer fzcd'),
    'file' => 'fzcd.admin.inc',
  );
  $items['admin/config/services/fzcd/import'] = array(
    'title' => 'Import',
    'description' => 'Configure the Free ZIP Code Database module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('fzcd_admin_form'),
    'access arguments' => array('administer fzcd'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'fzcd.admin.inc',
  );

  return $items;
}

/**
 * Returns state code by zip
 *
 * @param string $zip
 * @return string US two-letters state code (capitalized)
 */
function fzcd_get_state_by_zip($zip) {
  $state = db_select('fzcd')
      ->fields('fzcd', array('state'))
      ->condition('zip', $zip, '=')
      ->range(0, 1)
      ->execute()
      ->fetchField();

  return $state;
}

/**
 * Returns state by city
 *
 * @param string $city
 * @return array of US two-letters state code (capitalized)
 */
function fzcd_get_state_by_city($city) {
  $state = db_select('fzcd')
      ->fields('fzcd', array('state'))
      ->condition('city', $city, '=')
      ->groupBy('state')
      ->orderBy('state')
      ->execute()
      ->fetchAllKeyed(0, 0);

  return $state;
}

/**
 * Returns state by city
 *
 * @param string $city
 * @return array of zip codes
 */
function fzcd_get_zip_by_city($city) {
  $zip = db_select('fzcd')
      ->fields('fzcd', array('zip'))
      ->condition('city', $city, '=')
      ->execute()
      ->fetchAllKeyed(0, 0);

  return $zip;
}
